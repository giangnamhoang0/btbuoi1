import java.util.Scanner;

public class KiemTraSoNguyenTo {
    public static void main(String[] args) {
        int n;
        Scanner sc=new Scanner(System.in);
        System.out.println("Nhập số n:");
        n=sc.nextInt();
        int count=0;
        for (int i=2;i<=Math.sqrt(n);i++){
            if(n%i==0){
                count++;
            }
        }
        if(n<2){
            System.out.println(n+" không phải số nguyên tố");
        }
        else {
            if(count==0){
                System.out.println(n+" là số nguyên tố");
            }
            else {
                System.out.println(n+" không phải số nguyên tố");
            }
        }

    }
}
